const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CustomerModel = mongoose.model('User', new Schema({
    username: {
        type: Schema.Types.String,
        required: true,
        default: '',
        unique: true,
        index: true
    },
    password: {
        type: Schema.Types.String,
        required: true,
        default: ''
    },
    email: {
        type: Schema.Types.String,
        required: true,
        default: ''
    },
    fullName: {
        type: Schema.Types.String,
        default: ''
    },
    phone: {
        type: Schema.Types.String,
        default: ''
    },
    address: {
        type: Schema.Types.String,
        default: ''
    },
    avatar: {
        type: Schema.Types.String,
        default: ''
    },
    gender: {
        type: Schema.Types.String,
        default: '1'
    },
    status: {
        type: Schema.Types.String,
        default: 'active'
    },
    isDeleted: {
        type: Schema.Types.Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
}));

CustomerModel.STATIC.STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
}

module.exports = CustomerModel