const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserModel = mongoose.model('User', new Schema({
    username: {
        type: Schema.Types.String,
        required: true,
        default: '',
        unique: true,
        index: true
    },
    password: {
        type: Schema.Types.String,
        required: true,
        default: ''
    },
    email: {
        type: Schema.Types.String,
        required: true,
        default: ''
    },
    fullName: {
        type: Schema.Types.String,
        default: ''
    },
    phone: {
        type: Schema.Types.String,
        default: ''
    },
    status: {
        type: Schema.Types.String,
        default: 'active'
    },
    isDeleted: {
        type: Schema.Types.Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
}));

UserModel.index({ username })

UserModel.STATIC.STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
}

module.exports = UserModel