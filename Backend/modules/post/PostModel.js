const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PostModel = mongoose.model('User', new Schema({
    title: {
        type: Schema.Types.String,
        default: '',
    },
    description: {
        type: Schema.Types.String,
        default: ''
    },
    content: {
        type: Schema.Types.String,
        default: ''
    },
    image: {
        type: Schema.Types.String,
        default: ''
    },
    tags: {
        type: Schema.Types.Array,
    },
    featured: {
        type: Schema.Types.Boolean,
        required: true,
        default: false
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    updated_by: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    status: {
        type: Schema.Types.String,
        default: 'active'
    },
    isDeleted: {
        type: Schema.Types.Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
}));

PostModel.STATIC.STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
}

module.exports = PostModel