const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TypeModel = mongoose.model('User', new Schema({
    name: {
        type: Schema.Types.String,
        required: true,
        default: ''
    },
    code: {
        type: Schema.Types.String,
        required: true,
        default: '',
        unique: true,
        index: true
    },
    status: {
        type: Schema.Types.String,
        default: 'active'
    },
    isDeleted: {
        type: Schema.Types.Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
}));

TypeModel.STATIC.STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
}

module.exports = TypeModel