const express = require('express')
const app = express()

const port = process.env.PORT || 3000

app.use('/', (req, res) => {
    res.send('Welcome to api')
})

app.listen(port, err => {
    console.log('Server is running on port: ', port);
})